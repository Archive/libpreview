#include "preview-debug.h"

#if DEBUG_CACHE
void 
debug_dump_datalist (GQuark id, gpointer data, gpointer user_data)
{
	g_print ("   %s = %s\n", g_quark_to_string (id), (gchar*) data);
}

void
debug_dump_attributes (GData **data)
{
	g_print ("*** Dump thumbnail attribute list:\n");
	g_datalist_foreach (data, debug_dump_datalist, NULL);
}
#else

void debug_dump_datalist (GQuark id, gpointer data, gpointer user_data)
{
}

void
debug_dump_attributes (GData **data)
{
}

#endif


