/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-cache-async.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#ifndef _PREVIEW_CACHE_ASYNC_H_
#define _PREVIEW_CACHE_ASYNC_H_

#include <glib/gerror.h>
#include "preview-thumbnail.h"

typedef void (* PreviewCacheBooleanCallback) (PreviewThumbnail *thumb,
					      gboolean exists,
					      GError **err,
					      gpointer data);

typedef void (* PreviewCacheSimpleCallback) (PreviewThumbnail *thumb,
					     GError **err,
					     gpointer data);

void preview_cache_async_init              (void);

void preview_cache_async_shutdown          (void);

void preview_cache_async_cancel_jobs       (void);

void preview_cache_thumbnail_exists_async (PreviewThumbnail *thumb,
					   PreviewCacheBooleanCallback cb,
					   PreviewCacheSimpleCallback cancel_cb,
					   GError **err,
					   gpointer data);

void preview_cache_thumbnail_request_async (PreviewThumbnail *thumb,
					    PreviewCacheSimpleCallback cb,
  					    PreviewCacheSimpleCallback cancel_cb,
					    GError **err,
					    gpointer data);

void preview_cache_thumbnail_delete_async   (PreviewThumbnail *thumb, 
					     PreviewCacheBooleanCallback cb,
					     PreviewCacheSimpleCallback cancel_cb,
					     GError **err,
					     gpointer data);

void preview_cache_thumbnail_is_valid_async (PreviewThumbnail *thumb, 
					     PreviewCacheBooleanCallback cb,
					     PreviewCacheSimpleCallback cancel_cb,
					     GError **err,
					     gpointer data);

#endif /* _PREVIEW_CACHE_ASYNC_H_ */
