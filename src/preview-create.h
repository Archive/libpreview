/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-create.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#ifndef _PREVIEW_CREATE_H_
#define _PREVIEW_CREATE_H_

#include "preview-thumbnail.h"

GdkPixbuf* preview_create_pixbuf (GnomeVFSURI *src_uri,
				  const gchar *mime_type,
				  gint max_width,
				  gint max_height,
				  GData **attributes,
				  GError **err);


#endif /* _PREVIEW_CREATE_H_ */

