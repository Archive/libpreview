/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-create.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include "preview-create.h"
#include "preview-thumbnail-private.h"
#include "preview-pixbuf-util.h"
#include "preview-cache.h"
#include "preview-debug.h"

typedef GdkPixbuf* (* PreviewModuleCreatePixbuf) (GnomeVFSURI *src_uri,
						  const gchar *mime_type,
						  gint max_width,
						  gint max_height,
						  GData **attributes,
						  GError **err);

#define DEBUG_CREATE 0

static GdkPixbuf*
create_pixbuf_from_image (GnomeVFSURI *src_uri,
			  const gchar *mime_type,
			  gint max_width,
			  gint max_height,
			  GData **attributes,
			  GError **err)
{
	GdkPixbufLoader *loader;
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	GnomeVFSFileSize bytes_read;
	GdkPixbuf *pixbuf;
	GdkPixbuf *pb_thumb;
	gchar *buffer;
	GError *creation_error = NULL;
	gboolean creation_failed = FALSE;

	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	debug_msg ("create from image -->");

	loader = gdk_pixbuf_loader_new ();
	
	result = gnome_vfs_open_uri (&handle, src_uri, GNOME_VFS_OPEN_READ);
	if (result != GNOME_VFS_OK) {
		g_set_error (err, PREVIEW_CACHE_ERROR,
			     PREVIEW_CACHE_ERROR_CREATION_FAILED,
			     gnome_vfs_result_to_string (result));
		gdk_pixbuf_loader_close (loader, NULL);
		g_object_unref (loader);
		return NULL;
	}

	buffer = g_new0 (gchar, 4096);
	
	do {
		result = gnome_vfs_read (handle, buffer, 4096, &bytes_read);
		if (result != GNOME_VFS_OK && result != GNOME_VFS_ERROR_EOF) {
			g_set_error (err, PREVIEW_CACHE_ERROR,
				     PREVIEW_CACHE_ERROR_CREATION_FAILED,
				     gnome_vfs_result_to_string (result));
			creation_failed = TRUE;
		}

		if (bytes_read == 0) break;

		if (!gdk_pixbuf_loader_write (loader, buffer, bytes_read, &creation_error)) {
			g_set_error (err, PREVIEW_CACHE_ERROR,
				     PREVIEW_CACHE_ERROR_CREATION_FAILED,
				     creation_error->message);
			creation_failed = TRUE;
		}
	}
	while (bytes_read > 0 && creation_failed == FALSE);
		
	gnome_vfs_close (handle);
	if (creation_error != NULL) {
		g_error_free (creation_error);
	}
	gdk_pixbuf_loader_close (loader, NULL);

	pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
	if (pixbuf == NULL) {
		return NULL;
	}

	if (creation_failed) {
		g_object_unref (pixbuf);
		return NULL;
	}

	g_object_ref (pixbuf);
	g_object_unref (loader);
	g_free (buffer);

	buffer = g_new0 (gchar, 16);

	/* set additional attributes */ 
	g_snprintf (buffer, 16, "%i", gdk_pixbuf_get_width (pixbuf));
	g_datalist_set_data_full (attributes, TMS_KEY_IMAGE_WIDTH, 
				  g_strdup (buffer), g_free);
	g_snprintf (buffer, 16, "%i", gdk_pixbuf_get_height (pixbuf));
	g_datalist_set_data_full (attributes, TMS_KEY_IMAGE_HEIGHT, 
				  g_strdup (buffer), g_free); 
	g_free (buffer);

	pb_thumb = preview_pixbuf_scale (pixbuf, max_width, max_height);
	gdk_pixbuf_unref (pixbuf);

	debug_msg ("create from image <--");

	return pb_thumb;
}

#if 0
static GdkPixbuf*
create_pixbuf_from_svg  (GnomeVFSURI *src_uri,
			 const gchar *mime_type,
			 gint max_width,
			 gint max_height,
			 GData **attributes,
			 GError **err)
{
	return NULL;
}

static GdkPixbuf*
create_pixbuf_from_html (GnomeVFSURI *src_uri,
			 const gchar *mime_type,
			 gint max_width,
			 gint max_height,
			 GData **attributes,
			 GError **err)
{
	return NULL;
}

static GdkPixbuf*
create_pixbuf_from_text (GnomeVFSURI *src_uri,
			 const gchar *mime_type,
			 gint max_width,
			 gint max_height,
			 GData **attributes,
			 GError **err)
{
	return NULL;
}
#endif

typedef struct {
	gchar *mime_type;
	PreviewModuleCreatePixbuf create_func;
} PreviewModuleInfo;


static PreviewModuleInfo modules[] = {
	{ "image/bmp",               &create_pixbuf_from_image },
	{ "image/gif",               &create_pixbuf_from_image },
	{ "image/ico",               &create_pixbuf_from_image },
	{ "image/jpeg",              &create_pixbuf_from_image },
	{ "image/png",               &create_pixbuf_from_image },
	{ "image/x-portable-pixmap", &create_pixbuf_from_image },
	{ "image/x-cmu-raster",      &create_pixbuf_from_image },
	{ "image/x-tga",             &create_pixbuf_from_image },
	{ "image/tiff",              &create_pixbuf_from_image },
	{ "image/x-xbitmap",         &create_pixbuf_from_image },
	{ "image/x-xpixmap",         &create_pixbuf_from_image },
     /* { "image/svg", &create_pixbuf_from_svg }, */
     /*	{ "text/html", &create_pixbuf_from_html }, */
     /* { "text/", &create_pixbuf_from_text }, */
	{ NULL, NULL }
};

static PreviewModuleInfo*
get_module_for_mime_type (const gchar *mime_type)
{
	static GHashTable *modules_hash = NULL;
	int i;
	PreviewModuleInfo *module;

	if (!modules_hash) {
		modules_hash = g_hash_table_new (g_str_hash, g_str_equal);

		for (i = 0; modules[i].mime_type != NULL; i++)
			g_hash_table_insert (modules_hash,
					     (gpointer) modules [i].mime_type,
					     (gpointer) &modules[i]);	
	}

	module = g_hash_table_lookup (modules_hash, mime_type);

	return module;
}

GdkPixbuf* 
preview_create_pixbuf (GnomeVFSURI *src_uri,
		       const gchar *mime_type,
		       gint max_width,
		       gint max_height,
		       GData **attributes,
		       GError **err)
{
	PreviewModuleInfo *module;
	GdkPixbuf *pixbuf = NULL;

	g_return_val_if_fail (src_uri != NULL, NULL);
	g_return_val_if_fail (mime_type != NULL, NULL);
	g_return_val_if_fail (max_width > 0, NULL);
	g_return_val_if_fail (max_height > 0, NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	module = get_module_for_mime_type (mime_type);

	if (module == NULL) {
		g_set_error (err, PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_CREATION_NOT_SUPPORTED,
			     "No handler for file mimetype %s.", mime_type);
		return NULL;
	}
	
	pixbuf = (*(module->create_func)) (src_uri, mime_type, 
					   max_width, max_height,
					   attributes, err);
	return pixbuf;
}

