/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-thumbnail.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include <libgnomevfs/gnome-vfs.h>
#include "preview-thumbnail.h"
#include "preview-thumbnail-private.h"
#include "preview-pixbuf-util.h"
#include "md5.h"

#define MIN_THUMBNAIL_SIZE 10
#define MAX_THUMBNAIL_SIZE 256

struct _PreviewThumbnailPrivate {
	GnomeVFSURI             *src_uri;
	gchar                   *mime_type;
	PreviewThumbnailStatus  status;
	gint                    max_width;
	gint                    max_height;
	gchar                   *thumb_path;
	GdkPixbuf               *pixbuf;
	GData                   *attributes;
	PreviewCacheDirectory   dir;
};


static void preview_thumbnail_init       (PreviewThumbnail *thumb);
static void preview_thumbnail_class_init (PreviewThumbnailClass *klass);
static void preview_thumbnail_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void 
preview_thumbnail_init (PreviewThumbnail *thumb)
{
	PreviewThumbnailPrivate *priv;

	priv = g_new0 (PreviewThumbnailPrivate, 1);

	thumb->priv = priv;
}


static void 
preview_thumbnail_class_init (PreviewThumbnailClass *klass)
{
	GObjectClass *object_class = (GObjectClass*) klass;
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = preview_thumbnail_finalize;
}

static void 
preview_thumbnail_finalize   (GObject *object)
{
	PreviewThumbnail *thumb;
	PreviewThumbnailPrivate *priv;

	thumb = PREVIEW_THUMBNAIL (object);
	priv = thumb->priv;

	if (priv->src_uri)
	        gnome_vfs_uri_unref (priv->src_uri);
	priv->src_uri = NULL;

	if (priv->mime_type)
		g_free (priv->mime_type);
	priv->mime_type = NULL;

	if (priv->thumb_path)
		g_free (priv->thumb_path);
	priv->thumb_path = NULL;

	if (priv->pixbuf)
		gdk_pixbuf_unref (priv->pixbuf);
	priv->pixbuf = NULL;

	if (priv->attributes)
		g_datalist_clear (&priv->attributes);
	priv->attributes = NULL;

	g_free (priv);
	thumb->priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}


GType              
preview_thumbnail_get_type (void)
{
	static GType object_type = 0;
	
	if (!object_type)
	{
		static const GTypeInfo object_info =
		{
			sizeof (PreviewThumbnailClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) preview_thumbnail_class_init,
			NULL,   /* clas_finalize */
			NULL,   /* class_data */
			sizeof(PreviewThumbnail),
			0,      /* n_preallocs */
			(GInstanceInitFunc) preview_thumbnail_init,
		};
		
		object_type = g_type_register_static (G_TYPE_OBJECT,
						      "PreviewThumbnail",
						      &object_info, 0);
	}
	
	return object_type;
}

static void
preview_thumbnail_construct (PreviewThumbnail *thumb, 
			     GnomeVFSURI *uri,
			     gint max_width, 
			     gint max_height)
{
	PreviewThumbnailPrivate *priv;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	priv = thumb->priv;

	priv->src_uri = gnome_vfs_uri_ref (uri);
	priv->max_width = max_width;
	priv->max_height = max_height;
	priv->status = PREVIEW_THUMBNAIL_NOT_LOADED;
	priv->pixbuf = NULL;
	g_datalist_init (&priv->attributes);
	priv->dir = PREVIEW_CACHE_DIR_UNKNOWN;
}

PreviewThumbnail*
preview_thumbnail_new (gchar *txt_uri, 
		       gint max_width, 
		       gint max_height)
{
	PreviewThumbnail *thumb;
	GnomeVFSURI *uri;

	g_return_val_if_fail (txt_uri != NULL, NULL);

	uri = gnome_vfs_uri_new (txt_uri);
	thumb = preview_thumbnail_new_uri (uri, max_width, max_height);
	gnome_vfs_uri_unref (uri);

	return thumb;
}

PreviewThumbnail*
preview_thumbnail_new_uri (GnomeVFSURI *uri,
			   gint max_width,
			   gint max_height)
{
	PreviewThumbnail *thumb;

	g_return_val_if_fail (uri != NULL, NULL);
	g_return_val_if_fail (max_width >= MIN_THUMBNAIL_SIZE, NULL);
	g_return_val_if_fail (max_height >= MIN_THUMBNAIL_SIZE, NULL);
	g_return_val_if_fail (max_width <= MAX_THUMBNAIL_SIZE, NULL);
	g_return_val_if_fail (max_height <= MAX_THUMBNAIL_SIZE, NULL);

	thumb = g_object_new (PREVIEW_TYPE_THUMBNAIL, NULL);
	
	preview_thumbnail_construct (thumb, uri, max_width, max_height);

	return thumb;
}

PreviewThumbnailStatus
preview_thumbnail_get_status (PreviewThumbnail *thumb)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), PREVIEW_THUMBNAIL_FAILED);
	
	if (thumb->priv->status == PREVIEW_THUMBNAIL_LOADED &&
	    thumb->priv->dir == PREVIEW_CACHE_DIR_FAIL)
	{
		return PREVIEW_THUMBNAIL_FAILED;
	}
	else {
		return (thumb->priv->status);
	}
}

void 
preview_thumbnail_set_status (PreviewThumbnail *thumb,
			      PreviewThumbnailStatus status)
{
	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	
	thumb->priv->status = status;
}


static gchar*
preview_thumbnail_construct_path (PreviewThumbnail *thumb)
{
	PreviewThumbnailPrivate *priv;
	md5_state_t state;
	md5_byte_t digest[16];
	gchar *text_uri;
	gchar *path;
	gchar *ptr;
	gint i;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	priv = thumb->priv;
	text_uri = gnome_vfs_uri_to_string (priv->src_uri, GNOME_VFS_URI_HIDE_NONE);

	md5_init (&state);
	md5_append (&state, (const md5_byte_t *) text_uri, strlen (text_uri));
	md5_finish (&state, digest);

	path = g_new0 (gchar, 32+4+1); /* digest + '.png' + '\0' */
	ptr = path;
	for (i = 0; i < 16; i++, ptr = ptr + 2)
		g_snprintf (ptr, 3, "%02x", digest[i]);
			  
	path[32+0] = '.';  /* append ".png\0" */
	path[32+1] = 'p';
	path[32+2] = 'n';
	path[32+3] = 'g';
	path[32+4] = '\0';
	
	g_free (text_uri);

	return path;
}

G_CONST_RETURN gchar*
preview_thumbnail_get_path (PreviewThumbnail *thumb)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	if (thumb->priv->thumb_path == NULL) {
		thumb->priv->thumb_path = 
			preview_thumbnail_construct_path (thumb);
	}

	return thumb->priv->thumb_path;
}

GnomeVFSURI*
preview_thumbnail_get_orig_uri (PreviewThumbnail *thumb)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	return gnome_vfs_uri_ref (thumb->priv->src_uri);
}

static gchar*
obtain_mime_type (PreviewThumbnail *thumb)
{
	PreviewThumbnailPrivate *priv;
	gchar *result;
	GnomeVFSFileInfo *info;
	GnomeVFSResult vfs_result;
	
	priv = thumb->priv;
	
	info = gnome_vfs_file_info_new ();
	vfs_result = gnome_vfs_get_file_info_uri (priv->src_uri, info,
						  GNOME_VFS_FILE_INFO_GET_MIME_TYPE);

	if (vfs_result != GNOME_VFS_OK) {
		g_warning ("Couldn't obtain mime type: %s", 
			   gnome_vfs_result_to_string (vfs_result));
		return NULL;
	}

	result = g_strdup (gnome_vfs_file_info_get_mime_type (info));
	g_assert (result != NULL);

	gnome_vfs_file_info_unref (info);

	return result;
}

G_CONST_RETURN gchar*
preview_thumbnail_get_mime_type (PreviewThumbnail *thumb)
{
	PreviewThumbnailPrivate *priv;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	priv = thumb->priv;
	
	if (priv->mime_type == NULL) {
		priv->mime_type = preview_thumbnail_get_attribute (thumb, TMS_KEY_MIMETYPE);
	
		if (priv->mime_type == NULL) {
			priv->mime_type = obtain_mime_type (thumb);
		}
	}

	return priv->mime_type;
}

GdkPixbuf*
preview_thumbnail_get_pixbuf (PreviewThumbnail *thumb)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);
	
	if (thumb->priv->pixbuf != NULL) {
		return gdk_pixbuf_ref (thumb->priv->pixbuf);
	}
	else {
		return NULL;
	}
}

void               
preview_thumbnail_get_max_size (PreviewThumbnail *thumb, 
				gint *max_width, 
				gint *max_height)
{
	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	
	*max_width = thumb->priv->max_width;
	*max_height = thumb->priv->max_height;
}

void 
preview_thumbnail_set_pixbuf (PreviewThumbnail *thumb, 
			      GdkPixbuf *pixbuf)
{
	PreviewThumbnailPrivate *priv;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (pixbuf != NULL);

	priv = thumb->priv;

	if (priv->pixbuf) gdk_pixbuf_unref (priv->pixbuf);
	priv->status = PREVIEW_THUMBNAIL_LOADED;

	priv->pixbuf = gdk_pixbuf_ref (pixbuf);
}

void
preview_thumbnail_delete (PreviewThumbnail *thumb)
{
	PreviewThumbnailPrivate *priv;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	priv = thumb->priv;

	preview_thumbnail_clear_attributes (thumb);

	if (priv->pixbuf)
		gdk_pixbuf_unref (priv->pixbuf);
	priv->pixbuf = NULL;
	priv->status = PREVIEW_THUMBNAIL_NOT_LOADED;
}

void
preview_thumbnail_clear_attributes (PreviewThumbnail *thumb)
{
	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	g_datalist_clear (&thumb->priv->attributes);
	thumb->priv->attributes = NULL;
	g_datalist_init (&thumb->priv->attributes);
}

gchar*
preview_thumbnail_get_attribute (PreviewThumbnail *thumb, 
				 gchar *key)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);
	
	return g_datalist_get_data (&thumb->priv->attributes, key);
}

gboolean       
preview_thumbnail_attribute_exists (PreviewThumbnail *thumb, gchar *key)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), FALSE);

	return (g_datalist_get_data (&thumb->priv->attributes, key) != NULL);
}

GData**
preview_thumbnail_get_attribute_list (PreviewThumbnail *thumb)
{
        g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	return &thumb->priv->attributes;
}

void 
preview_thumbnail_set_dir (PreviewThumbnail *thumb, PreviewCacheDirectory dir)
{
	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (dir >= PREVIEW_CACHE_DIR_NORMAL && dir < PREVIEW_CACHE_DIR_END);

	thumb->priv->dir = dir;
}

PreviewCacheDirectory preview_thumbnail_get_dir (PreviewThumbnail *thumb)
{
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), PREVIEW_CACHE_DIR_UNKNOWN);
	
	return thumb->priv->dir;
}
