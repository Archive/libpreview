/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-pixbuf-util.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include "preview-pixbuf-util.h"

#define DEBUG_UTIL 0

void
preview_pixbuf_options_to_attributes (GdkPixbuf *pixbuf, GData **attributes)
{
	GQuark quark;
	gchar **options;
	gint n = 0;
	gchar *key;
	gchar *value;

	*attributes = NULL;

	g_return_if_fail (GDK_IS_PIXBUF (pixbuf));

        quark = g_quark_from_static_string ("gdk_pixbuf_options");
        options = g_object_get_qdata (G_OBJECT (pixbuf), quark);

        if (options == NULL) return;

	for (n = 0; options[2*n]; n++) {
		if (g_strncasecmp ("tEXt::", options[2*n], 6) == 0)
			key = g_strdup (options[2*n]+6);
		else
			key = g_strdup (options[2*n]);

		value = g_strdup (options[2*n+1]);
		
		g_datalist_set_data_full (attributes, key, value, g_free);
	}
}

typedef struct {
	gint n;
	gchar ***keys;
	gchar ***values;
} OptionStorage;


static void
construct_options (GQuark key_id, gpointer data, gpointer user_data)
{
	OptionStorage *storage;
	gchar ***keys;
	gchar ***values;
	gint n;

	storage = (OptionStorage*) user_data;
	
	keys = storage->keys;
	values = storage->values;
	n = storage->n;
	
	*keys = g_renew (gchar*, *keys, storage->n + 2);
	*values = g_renew (gchar*, *values, storage->n + 2);

	(*keys)[n] = g_strconcat ("tEXt::", g_quark_to_string (key_id), NULL);
	(*keys)[n+1] = NULL;

	(*values)[n] = g_strdup ((gchar*) data);
	(*values)[n+1] = NULL;

	storage->n++;
}

void 
preview_pixbuf_attributes_to_options (GData **attributes, /* in */ 
				      gchar ***keys,     /* out */
				      gchar ***values    /* out */)
{
	OptionStorage storage;

	*keys = *values = NULL;
	
	storage.n      = 0;
	storage.keys   = keys;
	storage.values = values;

	g_datalist_foreach (attributes, 
			    construct_options,
			    &storage);
	
}

GdkPixbuf*
preview_pixbuf_scale (GdkPixbuf *pixbuf, 
		      gint max_width, 
		      gint max_height)
{
	gdouble xfactor, yfactor;
	gint pb_width, pb_height;
	gint width, height;

	g_return_val_if_fail (GDK_IS_PIXBUF (pixbuf), NULL);

	pb_width = gdk_pixbuf_get_width  (pixbuf);
	pb_height = gdk_pixbuf_get_height (pixbuf);

	xfactor = (gdouble) max_width / (gdouble) pb_width;
	yfactor = (gdouble) max_height / (gdouble) pb_height;

	if (xfactor < yfactor) {
		width = max_width;
		height = (gint) pb_height * xfactor;
	}
	else {
		height = max_height;
		width = (gint) pb_width * yfactor;
	}

	return gdk_pixbuf_scale_simple (pixbuf,
					width, height,
					GDK_INTERP_BILINEAR);	
}

void 
preview_pixbuf_save (GdkPixbuf *pixbuf, 
		     const gchar *path,
		     gchar **keys, 
		     gchar **values)
{
	g_return_if_fail (GDK_IS_PIXBUF (pixbuf));
	g_return_if_fail (path != NULL);

#if DEBUG_UTIL
	g_print ("Save path: %s\n", path);
#endif

	gdk_pixbuf_savev (pixbuf, path, "png", 
			  keys, values,
			  NULL);
}

void 
preview_pixbuf_option_dump  (GdkPixbuf *pixbuf)
{
        GQuark  quark;
        gchar **options;
        gint n = 0;
 
        g_return_if_fail (GDK_IS_PIXBUF (pixbuf));

        quark = g_quark_from_static_string ("gdk_pixbuf_options");

        options = g_object_get_qdata (G_OBJECT (pixbuf), quark);

	g_print ("*** Dump pixbuf options: ");
	
	if (!options) {
		g_print ("No pixbuf options!\n");
		return;
	}
	else
		g_print ("\n");
	
	for (n = 0; options[2*n]; n++) {
		g_print ("  %s = %s\n", options[2*n], options[2*n+1]);
	}
}
