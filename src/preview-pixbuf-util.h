/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-pixbuf-util.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#ifndef _PREVIEW_PIXBUF_HELPER_H_
#define _PREVIEW_PIXBUF_HELPER_H_

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib.h>

void preview_pixbuf_options_to_attributes (GdkPixbuf *pixbuf, GData **attributes);

void preview_pixbuf_attributes_to_options (GData **attributes, /* in */ 
					   gchar ***keys,      /* out */
					   gchar ***values     /* out */);
GdkPixbuf *preview_pixbuf_scale (GdkPixbuf *pixbuf, 
				 gint max_width, 
				 gint max_height);
void preview_pixbuf_save (GdkPixbuf *pixbuf, 
			  const gchar *path,
			  gchar **keys, 
			  gchar **values);

void preview_pixbuf_option_dump (GdkPixbuf *pixbuf);

#endif /* _PREVIEW_PIXBUF_HELPER_H_ */
