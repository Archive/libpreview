/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-thumbnail-private.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include "preview-thumbnail.h"

typedef struct _PreviewCacheDirInfo PreviewCacheDirInfo;
struct _PreviewCacheDirInfo  {
	char *name;
	int   width;
	int   height;
};

const static PreviewCacheDirInfo cache_dir_info[] = {
	{ "normal", 128, 128 },
	{ "large", 256, 256 },
	{ "fail"G_DIR_SEPARATOR_S"libpreview", -1, -1 }
};

void preview_thumbnail_set_pixbuf (PreviewThumbnail *thumb, 
				   GdkPixbuf *pixbuf);
void preview_thumbnail_set_status (PreviewThumbnail *thumb,
				   PreviewThumbnailStatus status);

void preview_thumbnail_clear_attributes (PreviewThumbnail *thumb);

void preview_thumbnail_delete (PreviewThumbnail *thumb);

void preview_thumbnail_set_dir (PreviewThumbnail *thumb, PreviewCacheDirectory dir);

PreviewCacheDirectory preview_thumbnail_get_dir (PreviewThumbnail *thumb);
