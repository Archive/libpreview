/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-cache.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <glib.h>
#include "preview-cache.h"
#include "preview-thumbnail-private.h"
#include "preview-pixbuf-util.h"
#include "preview-create.h"
#include "preview-debug.h"

static void thumbnail_load (PreviewThumbnail *thumb, char *path, GError **err);
static void thumbnail_create (PreviewThumbnail *thumb, GError **err);
static gchar* thumbnail_get_full_path (PreviewThumbnail *thumb, 
				       PreviewCacheDirectory force_dir);


/*===============================================
 *  
 *     Private helper functions.
 *
 *------------------------------------------------*/

static gboolean
check_and_create_dir (gchar *path)
{
	GnomeVFSResult result;
	gchar *toplevel_dir;
	gchar *sep;

	g_return_val_if_fail (path != NULL, FALSE);

#if DEBUG_CACHE
	g_print ("check dir: %s\n", path);
#endif

	if (g_file_test (path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))
		return TRUE;
	
	toplevel_dir = g_strdup (path);
	sep = g_strrstr (toplevel_dir, G_DIR_SEPARATOR_S);
#if DEBUG_CACHE
	g_print ("%s\n", sep);
#endif
	if (sep == NULL) {
		g_free (toplevel_dir);
		return FALSE;
	}
	*sep = '\0';
	
	if (!check_and_create_dir (toplevel_dir)) {
		g_free (toplevel_dir);
		return  FALSE;
	}
	g_free (toplevel_dir);

	result = gnome_vfs_make_directory (path, 
					   GNOME_VFS_PERM_USER_READ  | 
					   GNOME_VFS_PERM_USER_WRITE |
					   GNOME_VFS_PERM_USER_EXEC);
	return (result == GNOME_VFS_OK);
}

GQuark
preview_cache_error_quark (void)
{
	static GQuark q = 0;
	if (q == 0)
		q = g_quark_from_static_string ("preview-cache-error-quark");
	
	return q;
}

static PreviewCacheDirectory 
thumbnail_get_cache_dir (PreviewThumbnail *thumb)
{
	int width, height, i;
	PreviewThumbnailStatus status;
	PreviewCacheDirectory dir = PREVIEW_CACHE_DIR_UNKNOWN;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), FALSE);
	
	preview_thumbnail_get_max_size (thumb, &width, &height);
	status = preview_thumbnail_get_status (thumb);

	if (status == PREVIEW_THUMBNAIL_FAILED) {
		dir = PREVIEW_CACHE_DIR_FAIL;
	}
	else {
		for (i = 0; i < PREVIEW_CACHE_DIR_END; i++) {
			if (width <= cache_dir_info[i].width && 
			    height <= cache_dir_info[i].height)
			{
				dir = i;
				break;
			}
		}
	}

	return dir;
}

static gboolean 
thumbnail_exists (PreviewThumbnail *thumb, PreviewCacheDirectory dir)
{
	gchar *path;
	gboolean result = FALSE;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), FALSE);

	path = thumbnail_get_full_path (thumb, dir);
	if (path != NULL) {
		result = g_file_test (path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR);
		g_free (path);
	}

	return result;
}

static gchar*
thumbnail_get_full_path (PreviewThumbnail *thumb, PreviewCacheDirectory dir)
{
	gchar *path;
	gchar *cache;
	const gchar *thumb_path = preview_thumbnail_get_path (thumb);
	
	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);
	g_return_val_if_fail (dir != PREVIEW_CACHE_DIR_END, NULL);
	
	if (dir == PREVIEW_CACHE_DIR_UNKNOWN)
		return NULL;

	cache = preview_cache_get_path ();
	path = g_build_filename (cache, cache_dir_info[dir].name, thumb_path, NULL);

	g_free (cache);

	return path;
}

/* This function assumes that the thumbnail exists. This should
 * be checked with preview_cache_thumbnail_exists before.
 */
static void
thumbnail_load (PreviewThumbnail *thumb, char *path, GError **err)
{
	GdkPixbuf *pixbuf;
	GdkPixbuf *tmp;
	gint max_width, max_height;
	gint width, height;
	GData **attributes;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (err == NULL || *err == NULL);
	g_return_if_fail (path != NULL);

	if (preview_thumbnail_get_status (thumb) != PREVIEW_THUMBNAIL_NOT_LOADED)
		return;

	/* load png image */
	pixbuf = gdk_pixbuf_new_from_file (path, err);
	if (pixbuf == NULL) {
		g_set_error (err, PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_THUMB_CORRUPTED,
			     "Couldn't load thumbnail.");
		preview_thumbnail_set_status (thumb, PREVIEW_THUMBNAIL_FAILED);
		return;
	}

	/* set TMS attributes */
	preview_thumbnail_clear_attributes (thumb);
	attributes = preview_thumbnail_get_attribute_list (thumb);
	preview_pixbuf_options_to_attributes (pixbuf, attributes);
	debug_dump_attributes (attributes);
	
	/* scale thumbnail if neccessary */
	width = gdk_pixbuf_get_width (pixbuf);
	height = gdk_pixbuf_get_height (pixbuf);
	preview_thumbnail_get_max_size (thumb, &max_width, &max_height);

	if ((width > max_width) || (height > max_height)) {
		tmp = preview_pixbuf_scale (pixbuf, max_width, max_height);
		gdk_pixbuf_unref (pixbuf);
		pixbuf = tmp;
	}
	
	preview_thumbnail_set_pixbuf (thumb, pixbuf);
	preview_thumbnail_set_status (thumb, PREVIEW_THUMBNAIL_LOADED);

	gdk_pixbuf_unref (pixbuf);
}

static gchar* 
generate_temp_file_name (void)
{
	gchar *str;

	str = g_new0 (gchar, 50);
	g_snprintf (str, 50, "libpreview-%i-%i.png", getpid (), g_random_int ());

	return str;
}

/* This function assumes that the thumbnail exists and is already loaded.
 */
static gboolean
thumbnail_is_valid (PreviewThumbnail *thumb, GError **err)
{
	GnomeVFSFileInfo *info;
	GnomeVFSResult vfs_result;
	GnomeVFSURI *src_uri;
	char *val;
	gint thumb_mtime;
	gboolean result = FALSE;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);
	g_return_val_if_fail (preview_thumbnail_get_status (thumb) == PREVIEW_THUMBNAIL_LOADED, FALSE);

	/* obtain saved thumbnail mtime */
	val = preview_thumbnail_get_attribute (thumb, TMS_KEY_MTIME);
	if (val == NULL) {
		g_set_error (err, PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_THUMB_CORRUPTED,
			     "Thumbnail has no Thumb::MTime key");
		return FALSE;
	}
	thumb_mtime = atoi (val);

	/* obtain original file mtime */
	src_uri = preview_thumbnail_get_orig_uri (thumb);
	info = gnome_vfs_file_info_new ();
	vfs_result = gnome_vfs_get_file_info_uri (src_uri, info,
						  GNOME_VFS_FILE_INFO_FIELDS_MTIME);
	if (vfs_result == GNOME_VFS_OK) {
		/* compare mtime of original file with thumbnail */
		result = (thumb_mtime == info->mtime);
	}
	else if (vfs_result == GNOME_VFS_ERROR_NOT_FOUND) {
		g_set_error (err, 
			     PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_FILE_NOT_FOUND,
			     "File not found: %s", 
			     gnome_vfs_uri_to_string (src_uri,
						      GNOME_VFS_URI_HIDE_NONE));
		result = FALSE;
	}
	else {
		g_set_error (err, 
			     PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_VFS,
			     "GnomeVFS error occured: %s.", 
			     gnome_vfs_result_to_string (vfs_result));
		result = FALSE;
	}

	/* clean up */
	gnome_vfs_file_info_unref (info);
	gnome_vfs_uri_unref (src_uri);
	
	return result;
}

static void
thumbnail_save (PreviewThumbnail *thumb, 
		PreviewCacheDirectory dir, 
		GdkPixbuf *pixbuf, 
		GData **attributes,
		GError **err)
{
	GnomeVFSResult vfs_result;
	char *path;
	char *tmp_file;
	char *tmp_path;
	char **keys;
	char **values;
	char *basedir;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (pixbuf != NULL);

	preview_pixbuf_attributes_to_options (attributes,    /* in */
					      &keys, &values /* out */);
	path = thumbnail_get_full_path (thumb, dir);
	basedir = g_path_get_dirname (path);
	if (!check_and_create_dir (basedir)) {
		g_set_error (err, PREVIEW_CACHE_ERROR,
			     PREVIEW_CACHE_ERROR_CREATION_FAILED,
			     "Couldn't create directory: %s\n", basedir);
		g_free (basedir);
		return;
	}

	tmp_file = generate_temp_file_name ();
	tmp_path = g_build_filename (g_path_get_dirname (path), tmp_file, NULL);

	preview_pixbuf_save (pixbuf, 
			     tmp_path,
			     keys, values);

	vfs_result = gnome_vfs_move (tmp_path, path, TRUE);
	g_assert (vfs_result == GNOME_VFS_OK);

	g_free (tmp_path);
	g_free (tmp_file);
	g_free (path);
	g_strfreev (keys);
	g_strfreev (values);
}

static void
thumbnail_create (PreviewThumbnail *thumb, GError **err)
{
	GError *create_err = NULL; 
	PreviewCacheDirectory dir;
	GdkPixbuf *pixbuf;
	int width, height ;
	GData **attributes;
	GnomeVFSURI *uri;
	GnomeVFSFileInfo *info;
	GnomeVFSResult vfs_result;
	char *mtime_str;
	char *size_str;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (err == NULL || *err == NULL);

	/* obtain mandatory values for pixbuf creating */
	dir = thumbnail_get_cache_dir (thumb);
	uri = preview_thumbnail_get_orig_uri (thumb);
	preview_thumbnail_clear_attributes (thumb);
	attributes = preview_thumbnail_get_attribute_list (thumb);

	/* create pixbuf for thumbnail */
	pixbuf = preview_create_pixbuf (uri,
					preview_thumbnail_get_mime_type (thumb),
					cache_dir_info[dir].width,
					cache_dir_info[dir].height,
					attributes,
					&create_err);
	if (pixbuf == NULL) {
		preview_thumbnail_set_status (thumb, PREVIEW_THUMBNAIL_FAILED);
			
		if (create_err != NULL) {
			g_propagate_error (err, create_err);

			if (create_err->code == PREVIEW_CACHE_ERROR_CREATION_NOT_SUPPORTED) {
				gnome_vfs_uri_unref (uri);
				return;
			}
		}

		debug_msg ("Creating dummy thumbnail.");
		pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, 1, 1);
		dir = PREVIEW_CACHE_DIR_FAIL;
	}

	/* add rest of TMS attributes */
	info = gnome_vfs_file_info_new ();
	vfs_result = gnome_vfs_get_file_info_uri (uri, info, GNOME_VFS_FILE_INFO_DEFAULT);
	if (vfs_result != GNOME_VFS_OK) {
		g_set_error (err,  PREVIEW_CACHE_ERROR,
			     PREVIEW_CACHE_ERROR_CREATION_FAILED,
			     gnome_vfs_result_to_string (vfs_result));
		preview_thumbnail_set_status (thumb, PREVIEW_THUMBNAIL_FAILED);
		g_object_unref (pixbuf);
		gnome_vfs_uri_unref (uri);
		gnome_vfs_file_info_unref (info);
		return;
	}
	if (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_MTIME) {
		mtime_str = g_new0 (char, 20);
		g_snprintf (mtime_str, 20, "%lu",  info->mtime);
		g_datalist_set_data_full (attributes, 
					  TMS_KEY_MTIME, 
					  mtime_str, 
					  g_free);
	}
	if (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_SIZE) {
		size_str = g_new0 (char, 20);
		g_snprintf (size_str, 20, "%lu", (long int)info->size);
		g_datalist_set_data_full (attributes,
					  TMS_KEY_SIZE,
					  size_str,
					  g_free);
	}
	g_datalist_set_data (attributes, 
			     TMS_KEY_SOFTWARE, 
			     "GNOME::libpreview");
	g_datalist_set_data_full (attributes, 
				  TMS_KEY_URI, 
				  gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE),
				  g_free);


	debug_dump_attributes (attributes);

	thumbnail_save (thumb, dir, pixbuf, attributes, err);

	if (dir != PREVIEW_CACHE_DIR_FAIL) {
		preview_thumbnail_get_max_size (thumb, &width, &height);
		if ((gdk_pixbuf_get_width (pixbuf) > width) || 
		    (gdk_pixbuf_get_height (pixbuf) > height)) 
		{
			GdkPixbuf *tmp;

			tmp = preview_pixbuf_scale (pixbuf, width, height);
			g_object_unref (pixbuf);
			pixbuf = tmp;
		}

		preview_thumbnail_set_pixbuf (thumb, pixbuf);
		preview_thumbnail_set_status (thumb, PREVIEW_THUMBNAIL_LOADED);
	}

	preview_thumbnail_set_dir (thumb, dir);

	g_object_unref (pixbuf);
	gnome_vfs_file_info_unref (info);
}

void
thumbnail_delete (PreviewThumbnail *thumb, char *path, GError **err)
{
	GnomeVFSResult vfs_result;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (err == NULL || *err == NULL);
	g_return_if_fail (path != NULL);

	vfs_result = gnome_vfs_unlink (path);
	if (vfs_result == GNOME_VFS_OK) {
		/* update thumbnail status */
		preview_thumbnail_delete (thumb);
	}
	else {
		g_set_error (err, 
			     PREVIEW_CACHE_ERROR, 
			     PREVIEW_CACHE_ERROR_VFS,
			     gnome_vfs_result_to_string (vfs_result));
	}
}

/*===============================================
 *  
 *     Public API functions.
 *
 *------------------------------------------------*/
gboolean
preview_cache_thumbnail_exists (PreviewThumbnail *thumb, PreviewCacheDirectory *result_dir)
{
	PreviewCacheDirectory dir;

	dir = thumbnail_get_cache_dir (thumb);
	if (thumbnail_exists (thumb, dir)) {
		if (result_dir != NULL) {
			*result_dir = dir;
		}
		return TRUE;
	}
	else if (thumbnail_exists (thumb, PREVIEW_CACHE_DIR_FAIL)) {
		if (result_dir != NULL) {
			*result_dir = PREVIEW_CACHE_DIR_FAIL;
		}
		return TRUE;
	}
	else {
		return FALSE;
	}
}

void     
preview_cache_thumbnail_request (PreviewThumbnail *thumb, GError **err)
{
	GError *valid_error = NULL;
	GError *load_error = NULL;
	gboolean thumb_exists = FALSE;
	gboolean create_thumb = TRUE;
	PreviewCacheDirectory dir;
	char *path; 

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (err == NULL || *err == NULL);

	thumb_exists = preview_cache_thumbnail_exists (thumb, &dir);

	if (thumb_exists) {
		debug_msg ("*** Thumbnail exists");

		path = thumbnail_get_full_path (thumb, dir);

		thumbnail_load (thumb, path, &load_error);

		if (load_error == NULL) {
			if (thumbnail_is_valid (thumb, &valid_error)) {
				debug_msg ("*** Thumbnail is valid");
				create_thumb = FALSE;
				preview_thumbnail_set_dir (thumb, dir);
			}
			else {
				thumbnail_delete (thumb, path, NULL);

				if (valid_error != NULL &&
				    valid_error->code == PREVIEW_CACHE_ERROR_FILE_NOT_FOUND) {
					preview_thumbnail_set_status (thumb, 
								      PREVIEW_THUMBNAIL_NOT_LOADED);
					create_thumb = FALSE;
					g_propagate_error (err, valid_error);
				}
				
				if (valid_error != NULL) {
					g_clear_error (&valid_error);
				}
			}

		}
		else {
			g_clear_error (&load_error);
		}

		g_free (path);
	}

	if (create_thumb) {
		debug_msg ("*** Create Thumbnail");
		thumbnail_create (thumb, err);
	}
}

void
preview_cache_thumbnail_delete (PreviewThumbnail *thumb, GError **err)
{
	gchar *path;
	PreviewCacheDirectory dir;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));
	g_return_if_fail (err == NULL || *err == NULL);

	path = preview_cache_get_thumbnail_full_path (thumb);
		
	if (preview_cache_thumbnail_exists (thumb, &dir)) {
		path = thumbnail_get_full_path (thumb, dir);
		thumbnail_delete (thumb, path, err);
		g_free (path);
	}
	else {
		g_set_error (err,
			     PREVIEW_CACHE_ERROR,
			     PREVIEW_CACHE_ERROR_THUMB_NOT_FOUND,
			     "Thumbnail not found.");
	}
}

gboolean
preview_cache_thumbnail_is_valid (PreviewThumbnail *thumb, GError **err)
{
	GError *load_err = NULL;
	gboolean result = FALSE;
	char *path;
	PreviewCacheDirectory dir;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (preview_cache_thumbnail_exists (thumb, &dir)) {
		path = thumbnail_get_full_path (thumb, dir);

		thumbnail_load (thumb, path, &load_err);
		if (load_err == NULL) {
			result = thumbnail_is_valid (thumb, err);
		}
		else {
			g_clear_error (&load_err);
		}

		g_free (path);
	}

	return result;
}

gchar*   
preview_cache_get_path (void)
{
	return g_build_filename (g_get_home_dir (), ".thumbnails", NULL);
}

gchar*   
preview_cache_get_thumbnail_full_path (PreviewThumbnail *thumb)
{
	PreviewCacheDirectory dir;

	g_return_val_if_fail (PREVIEW_IS_THUMBNAIL (thumb), NULL);

	dir = preview_thumbnail_get_dir (thumb);
	if (dir == PREVIEW_CACHE_DIR_UNKNOWN) {
		dir = thumbnail_get_cache_dir (thumb);
	}

	return thumbnail_get_full_path (thumb, dir);
}


PreviewThumbnail* 
preview_cache_get_thumbnail (char *path_to_cache_png)
{
	GdkPixbuf *pixbuf;
	char *cache_path;
	char *dir_path;
	PreviewThumbnail *thumb = NULL;
	int i;
	const PreviewCacheDirInfo *dir_info = NULL;

	cache_path = preview_cache_get_path ();

	for (i = 0; i < PREVIEW_CACHE_DIR_END; i++) {
		dir_path = g_build_filename (cache_path, cache_dir_info[i].name, NULL);
		
		if (g_strncasecmp (dir_path, path_to_cache_png, strlen (dir_path)) != 0) {
			dir_info = &cache_dir_info[i];
		}

		g_free (dir_path);
	}
	g_free (cache_path);

	if (dir_info == NULL) {
		return NULL;
	}
		
	if (!g_file_test (path_to_cache_png, G_FILE_TEST_EXISTS)) {
		return NULL;
	}

	pixbuf = gdk_pixbuf_new_from_file (path_to_cache_png, NULL);
	if (pixbuf != NULL) {
		const char *thumb_uri = gdk_pixbuf_get_option (pixbuf, "tEXt::"TMS_KEY_URI); 
								       
		if (thumb_uri != NULL) {
			int max_width; 
			int max_height;

			max_width = dir_info->width;
			max_height = dir_info->height;

			if (max_width == -1) max_width = cache_dir_info[PREVIEW_CACHE_DIR_NORMAL].width;
			if (max_height == -1) max_height = cache_dir_info[PREVIEW_CACHE_DIR_NORMAL].height;

			thumb = preview_thumbnail_new ((char*)thumb_uri,
						       max_width,
						       max_height);
		
			thumbnail_load (thumb, path_to_cache_png, NULL);
		}
		
		g_object_unref (pixbuf);
	}
	
	return thumb;
}
