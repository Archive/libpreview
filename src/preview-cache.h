/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-cache.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#ifndef _PREVIEW_CACHE_H_
#define _PREVIEW_CACHE_H_

#include <glib/gerror.h>
#include "preview-thumbnail.h"

typedef enum {
	PREVIEW_CACHE_ERROR_UNKNOWN,
	PREVIEW_CACHE_ERROR_FILE_NOT_FOUND,
	PREVIEW_CACHE_ERROR_THUMB_NOT_FOUND,
	PREVIEW_CACHE_ERROR_THUMB_CORRUPTED,
	PREVIEW_CACHE_ERROR_CREATION_FAILED,
	PREVIEW_CACHE_ERROR_CREATION_ERROR,
	PREVIEW_CACHE_ERROR_CREATION_NOT_SUPPORTED,
	PREVIEW_CACHE_ERROR_VFS
} PreviewCacheError;

#define PREVIEW_CACHE_ERROR preview_cache_error_quark ()

GQuark   preview_cache_error_quark             (void);

gchar*   preview_cache_get_path                (void);

gboolean preview_cache_thumbnail_exists        (PreviewThumbnail *thumb,
						PreviewCacheDirectory *dir);

void     preview_cache_thumbnail_request       (PreviewThumbnail *thumb, GError **err);

void     preview_cache_thumbnail_delete        (PreviewThumbnail *thumb, GError **err);

gboolean preview_cache_thumbnail_is_valid      (PreviewThumbnail *thumb, GError **err);

gchar*   preview_cache_get_thumbnail_full_path (PreviewThumbnail *thumb);

PreviewThumbnail* preview_cache_get_thumbnail  (char *path_to_cache_png);

#endif /* _PREVIEW_CACHE_H_ */
