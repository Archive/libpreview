/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-thumbnail.h 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#ifndef _PREVIEW_THUMBNAIL_H_
#define _PREVIEW_THUMBNAIL_H_

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomevfs/gnome-vfs.h>

G_BEGIN_DECLS
 
#define PREVIEW_TYPE_THUMBNAIL           (preview_thumbnail_get_type ())
#define PREVIEW_THUMBNAIL(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), PREVIEW_TYPE_THUMBNAIL, PreviewThumbnail))
#define PREVIEW_THUMBNAIL_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST((k), PREVIEW_TYPE_THUMBNAIL, PreviewThumbnailClass))

#define PREVIEW_IS_THUMBNAIL(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), PREVIEW_TYPE_THUMBNAIL))
#define PREVIEW_IS_THUMBNAIL_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), PREVIEW_TYPE_THUMBNAIL))
#define PREVIEW_THUMBNAIL_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((o), PREVIEW_TYPE_THUMBNAIL, PreviewThumbnailClass))

typedef struct _PreviewThumbnail         PreviewThumbnail;
typedef struct _PreviewThumbnailClass    PreviewThumbnailClass;
typedef struct _PreviewThumbnailPrivate  PreviewThumbnailPrivate;

typedef enum {
	PREVIEW_THUMBNAIL_NOT_LOADED,
	PREVIEW_THUMBNAIL_LOADED,
	PREVIEW_THUMBNAIL_FAILED
} PreviewThumbnailStatus;

typedef enum {
	PREVIEW_CACHE_DIR_NORMAL,
	PREVIEW_CACHE_DIR_LARGE,
	PREVIEW_CACHE_DIR_FAIL,
	/* rest is private, only for internal use */
	PREVIEW_CACHE_DIR_END,
	PREVIEW_CACHE_DIR_UNKNOWN
} PreviewCacheDirectory;

#define TMS_KEY_SOFTWARE     "Software"
#define TMS_KEY_MTIME        "Thumb::MTime"
#define TMS_KEY_URI          "Thumb::URI"
#define TMS_KEY_MIMETYPE     "Thumb::Mimetype"
#define TMS_KEY_SIZE         "Thumb::Size"
#define TMS_KEY_IMAGE_WIDTH  "Thumb::Image::Width"
#define TMS_KEY_IMAGE_HEIGHT "Thumb::Image::Height"

struct _PreviewThumbnail {
	GObject object;
	PreviewThumbnailPrivate *priv;
};

struct _PreviewThumbnailClass {
	GObjectClass parent_class;
};

GType                 preview_thumbnail_get_type       (void);
PreviewThumbnail     *preview_thumbnail_new            (gchar *txt_uri, 
							gint max_width, 
							gint max_height);
PreviewThumbnail     *preview_thumbnail_new_uri        (GnomeVFSURI *uri,
							gint width,
							gint height);
PreviewThumbnailStatus preview_thumbnail_get_status    (PreviewThumbnail *thumb);

GnomeVFSURI          *preview_thumbnail_get_orig_uri   (PreviewThumbnail *thumb);
G_CONST_RETURN gchar *preview_thumbnail_get_path       (PreviewThumbnail *thumb);
G_CONST_RETURN gchar *preview_thumbnail_get_mime_type  (PreviewThumbnail *thumb);
GdkPixbuf            *preview_thumbnail_get_pixbuf     (PreviewThumbnail *thumb);
void                  preview_thumbnail_get_max_size   (PreviewThumbnail *thumb, 
							gint *width, 
							gint *height);
gchar                *preview_thumbnail_get_attribute      (PreviewThumbnail *thumb, 
							    gchar *key);
gboolean              preview_thumbnail_attribute_exists   (PreviewThumbnail *thumb, 
							    gchar *key);
GData               **preview_thumbnail_get_attribute_list (PreviewThumbnail *thumb);

G_END_DECLS

#endif /* _PREVIEW_THUMBNAIL_H_ */
