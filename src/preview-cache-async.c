/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * src/preview-cache-async.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include <glib/gthread.h>
#include <glib/gqueue.h>
#include "preview-cache-async.h"
#include "preview-cache.h"

static GThread     *thread                     = NULL;
static gboolean     thread_running             = FALSE;
static GQueue      *jobs_done                  = NULL;
static GQueue      *jobs_waiting               = NULL;
static gint         dispatch_callbacks_id      = -1;
static gulong       cancel_stamp_limit         = 0;
static gulong       stamp_counter              = 1;

static GStaticMutex jobs_mutex = G_STATIC_MUTEX_INIT;

typedef enum {
	PREVIEW_JOB_EXISTS,
	PREVIEW_JOB_DELETE,
	PREVIEW_JOB_REQUEST,
	PREVIEW_JOB_IS_VALID
} PreviewJobKind;

typedef struct {
	PreviewThumbnail           *thumb;
	gulong                      time_stamp;
	gboolean                    canceled;
	PreviewJobKind              kind;
	PreviewCacheBooleanCallback bool_cb;
	PreviewCacheSimpleCallback  req_cb;
	PreviewCacheSimpleCallback  cancel_cb;
	gboolean                    result;
	gpointer                    data;
} PreviewJob;

#define DEBUG_ASYNC 0

static gint
dispatch_callback (gpointer data)
{
	PreviewJob *job;

#if DEBUG_ASYNC
	g_print ("*** dispatch callback called ***");
#endif

	job = NULL;

	g_static_mutex_lock (&jobs_mutex);
	if (!g_queue_is_empty (jobs_done)) {
		job = g_queue_pop_head (jobs_done);
	}
	else {
		g_queue_free (jobs_done);
		jobs_done = NULL;
		dispatch_callbacks_id = -1;
	}
	g_static_mutex_unlock (&jobs_mutex);	

	if (job == NULL) {
#if DEBUG_ASYNC
		g_print (" - shutdown\n");
#endif
		return FALSE;
	}

	if (job->canceled) {
		if (job->cancel_cb != NULL)
			(*job->cancel_cb) (job->thumb, NULL, job->data);
	}
	else {
		switch (job->kind) {
		case PREVIEW_JOB_EXISTS:
			if (job->bool_cb != NULL)
				(*job->bool_cb) (job->thumb, job->result, NULL, job->data);
			break;

		case PREVIEW_JOB_DELETE:
			if (job->bool_cb != NULL)
				(*job->bool_cb) (job->thumb, TRUE, NULL, job->data);
			break;
			
		case PREVIEW_JOB_REQUEST:
			if (job->req_cb != NULL)
				(*job->req_cb) (job->thumb, NULL, job->data); 
			break;
			
		case PREVIEW_JOB_IS_VALID:
			if (job->bool_cb != NULL)
				(*job->bool_cb) (job->thumb, job->result, NULL, job->data);
		default:
			break;
		}
	}

	g_free (job);

#if DEBUG_ASYNC
	g_print ("\n");
#endif
	
	return TRUE;
}

static gpointer
dispatch_thread (gpointer data)
{
	PreviewJob *job;
	gboolean finished = FALSE;

#if DEBUG_ASYNC
	g_print ("*** Start thread ***\n");
#endif	

	while (!finished) {

		g_static_mutex_lock (&jobs_mutex);
		job = g_queue_pop_head (jobs_waiting);
		g_static_mutex_unlock (&jobs_mutex);

		if (job->time_stamp < cancel_stamp_limit) {
			job->canceled = TRUE;
		}
		else {
			switch (job->kind) {
			case PREVIEW_JOB_EXISTS:
				job->result = preview_cache_thumbnail_exists (job->thumb, NULL);
				break;
				
			case PREVIEW_JOB_DELETE:
				preview_cache_thumbnail_delete (job->thumb, NULL);
				break;
				
			case PREVIEW_JOB_REQUEST:
				preview_cache_thumbnail_request (job->thumb, NULL);
				break;
				
			case PREVIEW_JOB_IS_VALID:
				job->result = preview_cache_thumbnail_is_valid (job->thumb, NULL);
				break;
			}
		}

		g_static_mutex_lock (&jobs_mutex);

		if (jobs_done == NULL) {
			jobs_done = g_queue_new ();
		}
		g_queue_push_tail (jobs_done, job);
		
		if (dispatch_callbacks_id == -1) {
			dispatch_callbacks_id = g_timeout_add_full (G_PRIORITY_DEFAULT_IDLE, 2,
								    dispatch_callback, NULL, NULL);
		}

		if (g_queue_is_empty (jobs_waiting)) {
			g_queue_free (jobs_waiting);
			jobs_waiting = NULL;
			thread_running = FALSE;
			finished = TRUE;
		}
			
		g_static_mutex_unlock (&jobs_mutex);
	}

#if DEBUG_ASYNC
	g_print ("*** Finish thread ***\n");
#endif	


	return NULL;
}

static void
real_cancel_jobs (gboolean immediately)
{
	if (!thread_running) return;

	cancel_stamp_limit = stamp_counter;

	if (immediately) {
		g_thread_join (thread);
	}
}

void 
preview_cache_async_shutdown (void)
{
	real_cancel_jobs (TRUE);
}

void
preview_cache_async_cancel_jobs (void)
{
	real_cancel_jobs (FALSE);
}

static void
add_job_to_queue (PreviewJob *job)
{
	if (!g_thread_supported ()) {
		g_thread_init (NULL);
	}

	job->time_stamp = stamp_counter++;
	job->canceled = FALSE;

	g_static_mutex_lock (&jobs_mutex);

	if (jobs_waiting == NULL) {
		jobs_waiting = g_queue_new ();
	}

	g_queue_push_tail (jobs_waiting, job);

	if (!thread_running) {
		thread = g_thread_create (dispatch_thread, NULL, TRUE, NULL);
		thread_running = TRUE;
	}

	g_static_mutex_unlock (&jobs_mutex);
}

void 
preview_cache_async_thumbnail_exists (PreviewThumbnail *thumb,
				      PreviewCacheBooleanCallback cb,
				      PreviewCacheSimpleCallback  cancel_cb,
				      GError **err,
				      gpointer data)
{
	PreviewJob *job;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	job = g_new0 (PreviewJob, 1);
	job->thumb     = thumb;
	job->kind      = PREVIEW_JOB_EXISTS;
	job->bool_cb   = cb;
	job->req_cb    = NULL;
	job->cancel_cb = cancel_cb;
	job->data      = data;

	add_job_to_queue (job);
}

void 
preview_cache_async_thumbnail_request (PreviewThumbnail *thumb,
				       PreviewCacheSimpleCallback cb,
				       PreviewCacheSimpleCallback cancel_cb,
				       GError **err,
				       gpointer data)
{
	PreviewJob *job;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	job = g_new0 (PreviewJob, 1);
	job->thumb     = thumb;
	job->kind      = PREVIEW_JOB_REQUEST;
	job->bool_cb   = NULL;
	job->req_cb    = cb;
	job->cancel_cb = cancel_cb;
	job->data      = data;

	add_job_to_queue (job);
}

void 
preview_cache_async_thumbnail_delete   (PreviewThumbnail *thumb, 
					PreviewCacheBooleanCallback cb,
					PreviewCacheSimpleCallback cancel_cb,
					GError **err,
					gpointer data)
{
	PreviewJob *job;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	job = g_new0 (PreviewJob, 1);
	job->thumb     = thumb;
	job->kind      = PREVIEW_JOB_DELETE;
	job->bool_cb   = cb;
	job->req_cb    = NULL;
	job->cancel_cb = cancel_cb;
	job->data      = data;

	add_job_to_queue (job);
}

void 
preview_cache_async_thumbnail_is_valid (PreviewThumbnail *thumb, 
					PreviewCacheBooleanCallback cb,
					PreviewCacheSimpleCallback cancel_cb,
					GError **err,
					gpointer data)
{
	PreviewJob *job;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	job = g_new0 (PreviewJob, 1);
	job->thumb     = thumb;
	job->kind      = PREVIEW_JOB_IS_VALID;
	job->bool_cb   = cb;
	job->req_cb    = NULL;
	job->cancel_cb = cancel_cb;
	job->data      = data;

	add_job_to_queue (job);
}

