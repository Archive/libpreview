/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * test/preview-test.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include <glib.h>
#include "libpreview.h"

static GMainLoop *loop;

static void
create_preview (gchar *file)
{
	PreviewThumbnail *thumb;
	GnomeVFSURI *uri;
	char *path;
	char *uri_str;
	GError *err = NULL;

	g_return_if_fail (file != NULL);

	if (!g_path_is_absolute (file)) {
		file = g_build_filename (g_get_current_dir (), file, NULL);
	}

	thumb = preview_thumbnail_new (file, 128, 128);
	preview_cache_thumbnail_request (thumb, &err);

	uri = preview_thumbnail_get_orig_uri (thumb);
	uri_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE),
	path = preview_cache_get_thumbnail_full_path (thumb);

	if (preview_thumbnail_get_status (thumb) == PREVIEW_THUMBNAIL_LOADED) {
		g_print ("+++ Thumbnail for %s successfully loaded.\n+++ Path: %s\n",
			 uri_str, path);
		if (err != NULL) {
			g_print ("+++ Error message: %s\n", err->message);
		}
	}
	else if (preview_thumbnail_get_status (thumb) == PREVIEW_THUMBNAIL_FAILED) {
		g_print ("--- Thumbnail for %s failed.\n--- Path: %s\n",
			 uri_str, path);
		if (err != NULL) {
			g_print ("--- Reason: %s\n", err->message);
		}
		else {
			g_print ("--- Reason: Unknown\n");
		}
	}
	else {
		g_print ("??? Thumbnail for %s not loaded.\n",
			 uri_str);
	}

	g_print ("\n");

	if (err != NULL)
		g_error_free (err);
	
	gnome_vfs_uri_unref (uri);
	g_free (uri_str);
	g_free (path);
	g_object_unref (G_OBJECT (thumb));
}

static gint
do_test (gpointer data)
{
	GList *files;

	files = (GList*) data;

	for (; files; files = files->next)
		create_preview ((gchar*) files->data);

	g_main_loop_quit (loop);

	return FALSE;
}


int
main (int argc, char *argv[]) 
{
	GList *files = NULL;
	gint i;

	g_type_init ();
	if (!gnome_vfs_init ()) {
		g_error ("Couldn't initialize gnome-vfs.");
		return 1;
	}
		
	if (argc > 1) {
		for (i = 1; i < argc; i++)
			files = g_list_append (files, argv[i]);
	}
	else {
		g_print ("Usage: preview-test [URI] ...\n");
		return 1;
	}

	loop = g_main_loop_new (NULL, FALSE);
	
	g_idle_add (do_test, files);

	g_main_loop_run (loop);
	
	g_main_loop_unref (loop);
	g_list_free (files);

	return 0;
}

