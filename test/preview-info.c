/* libpreview - A thumbnailing library.
 *
 * Copyright (C) 2002, Jens Finke <jens@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * test/preview-info.c 
 *
 * Authors:
 *   Jens Finke <jens@gnome.org>
 *
 */
#include <glib.h>
#include "libpreview.h"

static void
dump_datalist (GQuark id, gpointer data, gpointer user_data)
{
	const char *key = g_quark_to_string (id);
	if (g_strcasecmp (key, TMS_KEY_SOFTWARE) != 0 &&
	    g_strcasecmp (key, TMS_KEY_URI) != 0) {
		g_print ("      %s: %s\n", key, (gchar*) data);
	}
}

static void
dump_thumb_info (PreviewThumbnail *thumb)
{
	GData **attributes;
	PreviewThumbnailStatus status;
	char *status_str;
	GdkPixbuf *pixbuf;
	GError *err = NULL;
	gboolean valid;
	char *attr;

	g_return_if_fail (PREVIEW_IS_THUMBNAIL (thumb));

	/* original file */
	attr = preview_thumbnail_get_attribute (thumb, TMS_KEY_URI);
	if (attr != NULL) {
		g_print ("    Original file: %s\n", attr);
	}
	else {
		g_print ("    Original file: Unknow(?)\n");
	}
	
	/* thumbnail status */
	status = preview_thumbnail_get_status (thumb);
	switch (status) {
	case PREVIEW_THUMBNAIL_NOT_LOADED:
		status_str = "not loaded";
		break;
		
	case PREVIEW_THUMBNAIL_LOADED:
		status_str = "loaded";
		break;

	case PREVIEW_THUMBNAIL_FAILED:
		status_str = "failed";
		break;

	default:
		status_str = "unknown";
	}
	g_print ("    Status: %s\n", status_str);

	/* valid state */
	valid = preview_cache_thumbnail_is_valid (thumb, &err);
	if (err != NULL)
		g_print ("    Thumbnail valid: %s - Reason: %s\n", valid ? "yes" : "no", err->message);
	else 
		g_print ("    Thumbnail valid: %s\n", valid ? "yes" : "no");
		 
	/* pixbuf size */
	pixbuf = preview_thumbnail_get_pixbuf (thumb);
	if (pixbuf) {
		g_print ("    Thumbnail size: %ix%i (width x height)\n", 
			 gdk_pixbuf_get_width (pixbuf),
			 gdk_pixbuf_get_height (pixbuf));
	}

	/* creator */
	attr = preview_thumbnail_get_attribute (thumb, TMS_KEY_SOFTWARE);
	if (attr != NULL) {
		g_print ("    Thumbnail creator: %s\n", attr);
	}
	
	/* dump rest of attributes */
	g_print ("    Additional thumbnail attributes:\n");
	attributes = preview_thumbnail_get_attribute_list (thumb);
	g_datalist_foreach (attributes, dump_datalist, NULL);

	g_print ("\n");
}

int
main (int argc, char *argv[]) 
{
	PreviewThumbnail *thumb;
	gint i;

	g_type_init ();
	if (!gnome_vfs_init ()) {
		g_error ("Couldn't initialize gnome-vfs.");
		return 1;
	}


	if (argc < 2) {
		g_print ("Usage: preview-info [thumb_path] ...\n");
		return 1;
	}

	for (i = 1; i < argc; i++) {
		thumb = preview_cache_get_thumbnail (argv[i]);
		if (thumb != NULL) {
			g_print ("+++ Thumbnail information for: %s\n", argv[i]); 
			dump_thumb_info (thumb);
			g_object_unref (thumb);
		}
		else {
			g_print ("--- Couldn't get thumbnail object for: %s\n", argv[i]);
		}
	}

	return 0;
}
