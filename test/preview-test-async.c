#include <glib.h>
#include "libpreview.h"

static GMainLoop *loop;
static int n_files;
static int cancel_after_ms = 0;

static void
dump_datalist (GQuark id, gpointer data, gpointer user_data)
{
	g_print ("   %s = %s\n", g_quark_to_string (id), (gchar*) data);
}

static void
dump_attributes (GData **data)
{
	g_print ("*** Dump thumbnail attribute list:\n");
	g_datalist_foreach (data, dump_datalist, NULL);
}

static void
thumbnail_request_callback (PreviewThumbnail *thumb, GError **err, gpointer data)
{
	GnomeVFSURI *uri;
	char *uri_str;
	char *path;

	uri = preview_thumbnail_get_orig_uri (thumb);
	uri_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE),
	path = preview_cache_get_thumbnail_full_path (thumb);

	if (preview_thumbnail_get_status (thumb) == PREVIEW_THUMBNAIL_LOADED) {
		g_print ("+++ Thumbnail for %s successfully loaded.\n+++ Path: %s\n",
			 uri_str, path);
		if (err != NULL && *err != NULL) {
			g_print ("+++ Error message: %s\n", (*err)->message);
		}
	}
	else if (preview_thumbnail_get_status (thumb) == PREVIEW_THUMBNAIL_FAILED) {
		g_print ("--- Thumbnail for %s failed.\n--- Path: %s\n",
			 uri_str, path);
		if (err != NULL && *err != NULL) {
			g_print ("--- Reason: %s\n", (*err)->message);
		}
		else {
			g_print ("--- Reason: Unknown\n");
		}
	}
	else {
		g_print ("??? Thumbnail for %s not loaded.\n",
			 uri_str);
	}

	g_object_unref (G_OBJECT (thumb));
	gnome_vfs_uri_unref (uri);
	g_free (uri_str);
	g_free (path);

	n_files--;
	if (n_files == 0) {
		g_print ("**** ALL FILES PROCESSED ****\n");
		g_main_loop_quit (loop);
	}
}

static void
thumbnail_cancel_callback (PreviewThumbnail *thumb, GError **err, gpointer data)
{
	GnomeVFSURI *uri;
	char *uri_str;

	uri = preview_thumbnail_get_orig_uri (thumb);
	uri_str = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE),

	g_print (" >>> Thumbnail creation canceled for %s \n", uri_str);

	g_object_unref (G_OBJECT (thumb));
	gnome_vfs_uri_unref (uri);
	g_free (uri_str);

	n_files--;
	if (n_files == 0) {
		g_print ("**** ALL FILES PROCESSED ****\n");
		g_main_loop_quit (loop);
	}
}

static gboolean
cancel_thumbnail_creation (gpointer data)
{
	preview_cache_async_cancel_jobs ();
	return FALSE;
}

static gint
do_test (gpointer data)
{

	GList *files;
	PreviewThumbnail *thumb;

	files = (GList*) data;

	n_files = g_list_length (files);

	for (; files; files = files->next) {
		GError **err;

		thumb = preview_thumbnail_new (files->data, 128, 128);
		preview_cache_async_thumbnail_request (thumb, 
						       thumbnail_request_callback,
						       thumbnail_cancel_callback,
						       NULL,
						       NULL);
	}

	if (cancel_after_ms > 0) 
		g_timeout_add (cancel_after_ms, cancel_thumbnail_creation, NULL);

	return FALSE;
}

int
main (int argc, char *argv[]) 
{
	GList *files = NULL;
	gint i;

	g_type_init ();
	if (!gnome_vfs_init ()) {
		g_error ("Couldn't initialize gnome-vfs.");
		return 1;
	}
		
	if (argc > 1) {
		for (i = 1; i < argc; i++) {
			if (!g_strcasecmp (argv[i], "--cancel")) {
				if ((i+1) < argc) {
					cancel_after_ms = atoi (argv [++i]);
				}
			}
			else {
				files = g_list_append (files, argv[i]);
			}
		}
	}
		
	if (files == NULL) {
		g_print ("Usage: preview-test-async [--cancel <ms>] [URI] ...\n");
		return 1;
	}

	loop = g_main_loop_new (NULL, FALSE);
	
	g_idle_add (do_test, files);

	g_main_loop_run (loop);
	
	g_list_free (files);
	g_main_loop_unref (loop);

	return 0;
}
